//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: SteppingAction.hh 74483 2013-10-09 13:37:06Z gcosmo $
//
/// \file SteppingAction.hh
/// \brief Definition of the SteppingAction class

#ifndef SteppingAction_h
#define SteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include <vector>

class EventAction;

class G4LogicalVolume;

/// Stepping action class
/// 

class SteppingAction : public G4UserSteppingAction
{
  public:
    SteppingAction(EventAction* eventAction);
    virtual ~SteppingAction();

    // method from the base class
    virtual void UserSteppingAction(const G4Step*);

  private:
    EventAction*  fEventAction;
    G4LogicalVolume* fScoringVolume;

    double damageFunction(double e);

    // damage function for electrons / 95 MeVmb (neutron equivalent)
    // http://www.sr-niel.org/Simulation/516481niel_e.html

    const std::vector<double> df_e = {
        00000.10,  00000.15,  00000.20,  00000.25,  00000.30,  00000.35,  00000.40,
        00000.45,  00000.50,  00000.55,  00000.60,  00000.65,  00000.70,  00000.75,
        00000.80,  00000.85,  00000.90,  00000.95,  00001.00,  00001.50,  00002.00,
        00002.50,  00003.00,  00003.50,  00004.00,  00004.50,  00005.00,  00005.50,
        00006.00,  00006.50,  00007.00,  00007.50,  00008.00,  00008.50,  00009.00,
        00009.50,  00010.00,  00015.00,  00020.00,  00025.00,  00030.00,  00035.00,
        00040.00,  00045.00,  00050.00,  00055.00,  00060.00,  00065.00,  00070.00,
        00075.00,  00080.00,  00085.00,  00090.00,  00095.00,  00100.00,  00150.00,
        00200.00,  00250.00,  00300.00,  00350.00,  00400.00,  00450.00,  00500.00,
        00550.00,  00600.00,  00650.00,  00700.00,  00750.00,  00800.00,  00850.00,
        00900.00,  00950.00,  01000.00,  01500.00,  02000.00,  02500.00,  03000.00,
        03500.00,  04000.00,  04500.00,  05000.00,  05500.00,  06000.00,  06500.00,
        07000.00,  07500.00,  08000.00,  08500.00,  09000.00,  09500.00,  10000.00
    }; // MEV

    const std::vector<double> df_n = {
        0.        ,  0.        ,  0.        ,  0.00121725,  0.00287333,
        0.00419408,  0.00531324,  0.00629959,  0.00719216,  0.00801404,
        0.00878142,  0.00950412,  0.01018902,  0.01084152,  0.01146603,
        0.01206549,  0.01264189,  0.01319815,  0.01373576,  0.01831697,
        0.02190249,  0.02484485,  0.02733258,  0.02948252,  0.03137078,
        0.03305136,  0.03456304,  0.0359348 ,  0.03718824,  0.03834103,
        0.03940691,  0.0403967 ,  0.04132021,  0.0421848 ,  0.04299686,
        0.04376178,  0.04448399,  0.05002946,  0.05370679,  0.05633837,
        0.05830715,  0.05981441,  0.06099273,  0.06192557,  0.06266693,
        0.06326591,  0.06374705,  0.06413983,  0.06445896,  0.06471917,
        0.06493519,  0.06511685,  0.06526414,  0.06538197,  0.06548507,
        0.06592203,  0.06598586,  0.06597113,  0.06594167,  0.06590731,
        0.06587785,  0.06584839,  0.06582384,  0.0658042 ,  0.06578456,
        0.06576984,  0.06575511,  0.06574038,  0.06573056,  0.06572074,
        0.06571092,  0.0657011 ,  0.06569128,  0.06564218,  0.06561273,
        0.065598  ,  0.06558327,  0.06557836,  0.06556854,  0.06556363,
        0.06555872,  0.06555872,  0.06555381,  0.06555381,  0.0655489 ,
        0.0655489 ,  0.0655489 ,  0.06554399,  0.06554399,  0.06554399,
        0.06554399}; //  niel / 95 MeVmb = 1 MeV neutron equivalent



};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
