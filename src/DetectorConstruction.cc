//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file DetectorConstruction.cc
/// \brief Implementation of the DetectorConstruction class
//
// $Id: DetectorConstruction.cc 98257 2016-07-04 17:39:46Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
        : G4VUserDetectorConstruction()
{
    fWorldSize = 20*cm;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // Options:
    // TODO: Consider config file?
    int n_collimators = 1;
    //
    // define materials
    //
    G4Material* Air =
            G4NistManager::Instance()->FindOrBuildMaterial("G4_AIR");
    G4NistManager* nist = G4NistManager::Instance();

    G4Material* SiO2 = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");

    G4Material* Al = nist->FindOrBuildMaterial("G4_Al");

    //
    // World
    //
    G4Box*
            solidWorld = new G4Box("World",                          //its name
                                   fWorldSize/2,fWorldSize/2,fWorldSize/2);//its size

    G4LogicalVolume*
            logicWorld = new G4LogicalVolume(solidWorld,             //its solid
                                             Air,                    //its material
                                             "World");               //its name
    G4VPhysicalVolume*
            physiWorld = new G4PVPlacement(0,                      //no rotation
                                           G4ThreeVector(),        //at (0,0,0)
                                           logicWorld,             //its logical volume
                                           "World",                //its name
                                           0,                      //its mother  volume
                                           false,                  //no boolean operation
                                           0);                     //copy number
/////////////////////
    G4double a, z;
    G4double density;
    G4int    ncomponents;
    G4bool checkOverlaps = true;

    G4Material* Cu = new G4Material("Copper" , z=29., a = 63.550*g/mole, density = 8.96*g/cm3);
    G4Material* Zn = new G4Material("Zinc"   , z=30., a = 65.409*g/mole, density = 7.14*g/cm3);

    G4Material* Brass= new G4Material("Brass", density= 8.5*g/cm3, ncomponents=2);
    Brass->AddMaterial(Cu  , 70*perCent);
    Brass->AddMaterial(Zn  , 30*perCent);


    G4double container_x = 14;
    G4double container_y = 50;
    G4double container_z = 14;

    G4Box *containerBox =
            new G4Box("Container Box",                //name
                      container_x/2.,
                      container_y/2.,
                      container_z/2.);


/*  new G4PVPlacement(0,                       //no rotation
                    posContainer,            //at position
                    logicContainer,          //its logical volume
                    "Container",             //its name
                    logicWorld,              //its mother  volume
                    false,                   //no boolean operation
                    0,                       //copy number
                    checkOverlaps);          //overlaps checking
*/
    G4RotationMatrix* rotationMatrix = new G4RotationMatrix();
    rotationMatrix->rotateX(90.*deg);

    G4VSolid* hole1 =
            new G4Tubs("hole1",
                       0.,                           // inner radius
                       6.7/2.,                       // outer radius
                       4,                            // height
                       0.*deg,                       // rotation
                       360.*deg);

    G4VSolid* hole2 =
            new G4Tubs("hole2",
                       0.,                           // inner radius
                       1.57/2.,                       // outer radius
                       1.68,                            // height
                       0.*deg,                       // rotation
                       360.*deg);

    G4ThreeVector translation1(0, -container_y/2.,      0);
    G4ThreeVector translation2(0, -container_y/2. + 4 , 0);

    G4SubtractionSolid *boxplushole1 = new G4SubtractionSolid("Box+Hole1",
                                                              containerBox, hole1, rotationMatrix, translation1);

    G4SubtractionSolid *boxplushole2 = new G4SubtractionSolid("Box+Hole2",
                                                              boxplushole1, hole2, rotationMatrix, translation2);


    G4LogicalVolume* logicContainer =
            new G4LogicalVolume(boxplushole2,
                                Brass,
                                "Container");

    new G4PVPlacement(0,
                      G4ThreeVector(0, container_y/2., 0),
                      logicContainer,
                      "Container",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);

    if(n_collimators >= 1) {
        G4Box *collimator1Box =
                new G4Box("Collimator 1 Box",                //name
                          container_x/2.,
                          20./2.,
                          container_z/2.);

        G4VSolid* holecol =
                new G4Tubs("hole1",
                           0.,                           // inner radius
                           6.7/2.,                       // outer radius
                           20.,                            // height
                           0.*deg,                       // rotation
                           360.*deg);

        G4SubtractionSolid *collimator1plushole = new G4SubtractionSolid("Collimator 1 + Hole1",
                                                                         collimator1Box,
                                                                         holecol,
                                                                         rotationMatrix,
                                                                         G4ThreeVector(0,0,0));

        G4LogicalVolume* logicCollimator1 =
                new G4LogicalVolume(collimator1plushole,
                                    Brass,
                                    "Collimator 1");

        new G4PVPlacement(0,
                          G4ThreeVector(0, - 20./2., 0),
                          logicCollimator1,
                          "Collimator 1",
                          logicWorld,
                          false,
                          0,
                          checkOverlaps);
    }

    //
    if(n_collimators >= 2) {
        G4Box *collimator2Box =
                new G4Box("Collimator 2 Box",                //name
                          container_x/2.,
                          20./2.,
                          container_z/2.);

        G4VSolid* holecol2 =
                new G4Tubs("hole2",
                           0.,                           // inner radius
                           2./2.,                       // outer radius
                           20./2.,                  // height
                           0.*deg,                       // rotation
                           360.*deg);
        G4SubtractionSolid *collimator2plushole = new G4SubtractionSolid("Collimator 1 + Hole1",
                                                                         collimator2Box,
                                                                         holecol2,
                                                                         rotationMatrix,
                                                                         G4ThreeVector(0,0,0));

        G4LogicalVolume* logicCollimator2 =
                new G4LogicalVolume(collimator2plushole,
                                    Brass,
                                    "Collimator 2");

        new G4PVPlacement(0,
                          G4ThreeVector(0, - 20 - 20./2., 0),
                          logicCollimator2,
                          "Collimator 2",
                          logicWorld,
                          false,
                          0,
                          checkOverlaps);
    }

    // SiPM
    //
    G4Material *BADGE;
    // Fibres: epoxy resin; in first approximation Bisphenol A diglycidyl ether (BADGE)
    std::vector<G4String> BADGE_elements = { "C", "H", "O" };
    std::vector<G4int> BADGE_nbAtoms = { 21, 24, 4 };
    G4double BADGE_density = 1.15*g/cm3; // Polytec EP 601
    BADGE = nist->ConstructNewMaterial("BADGE", BADGE_elements, BADGE_nbAtoms, BADGE_density);

    G4Material* Silicon = nist->FindOrBuildMaterial("G4_Si");

    //
    //  Structure of little pcb with sipm
    //
    //
    //       EEEEEE       Epoxy
    //        oo          SiO2
    //        ss          Silicon (active)
    //       SSSSSS       Support (?)
    //  ################## PCB
    //






    G4double epoxythickness = 0.3;
    G4double sipmthickness = 0.3;
    G4double oxidethickness = 100 * nm;
    G4double sipmsupportthickness = 0.85 - epoxythickness - sipmthickness - oxidethickness;


    G4VSolid* solidEpoxy = new G4Box(
            "solidEpoxy",
            2.625/2., 2.1/2.,
            epoxythickness/2.);

    G4LogicalVolume * volumeEpoxy = new G4LogicalVolume(
            solidEpoxy,
            BADGE,
            "volumeEpoxy");

    G4VSolid* solidOxide      = new G4Box(
            "solidOxide",
            1.3/2., 1.3/2.,
            oxidethickness/2.);

    G4LogicalVolume * volumeOxide = new G4LogicalVolume(
            solidOxide,
            SiO2,
            "volumeOxide");

    G4VSolid* solidSilicon      = new G4Box(
            "solidSilicon",
            1.3/2., 1.3/2.,
            sipmthickness/2.);

    G4LogicalVolume * volumeSilicon = new G4LogicalVolume(
            solidSilicon,
            Silicon,
            "volumeSilicon");

    G4VSolid *solidSupport = new G4Box(
            "solidSupport",
            2.625/2., 2.1/2.,
            sipmsupportthickness/2.);

    G4LogicalVolume * volumeSupport = new G4LogicalVolume(
            solidSupport,
            Silicon,          // ?
            "volumeSupport");


    G4double sipmoffset = -n_collimators*20.;
    if(n_collimators >= 2)
        sipmoffset -= sipmsupportthickness + sipmthickness + oxidethickness + epoxythickness;


    new G4PVPlacement(rotationMatrix,
                      G4ThreeVector(0, sipmoffset + sipmsupportthickness/2., 0),
                      volumeSupport,
                      "support",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);

    new G4PVPlacement(rotationMatrix,
                      G4ThreeVector(0.26, sipmoffset + sipmsupportthickness + sipmthickness / 2., 0),
                      volumeSilicon,
                      "Silicon",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);

    new G4PVPlacement(rotationMatrix,
                      G4ThreeVector(0.26, sipmoffset + sipmsupportthickness + sipmthickness + oxidethickness / 2., 0),
                      volumeOxide,
                      "Oxide",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);

    new G4PVPlacement(rotationMatrix,
                      G4ThreeVector(0, sipmoffset + sipmsupportthickness + sipmthickness + oxidethickness + epoxythickness / 2., 0),
                      volumeEpoxy,
                      "Epoxy",
                      logicWorld,
                      false,
                      0,
                      checkOverlaps);


    fScoringVolume = volumeSilicon;


/////////////////////////
    //
    //always return the physical World
    //

    return physiWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
