//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file RunAction.cc
/// \brief Implementation of the RunAction class
//
// $Id: RunAction.cc 98257 2016-07-04 17:39:46Z gcosmo $
// 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "RunAction.hh"
#include "Run.hh"
#include "PrimaryGeneratorAction.hh"
#include "HistoManager.hh"
#include "DetectorConstruction.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include <iomanip>
#include "G4AccumulableManager.hh"

#include "TH1.h"
#include "TFile.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction(PrimaryGeneratorAction* kin)
:G4UserRunAction(),
 fPrimary(kin), fRun(0), fHistoManager(0), fEdep(0.), fEdep2(0.), nhits(0), niel(0), fdose(0)
{
  fHistoManager = new HistoManager();

  // add new units for dose
  //
  const G4double milligray = 1.e-3*gray;
  const G4double microgray = 1.e-6*gray;
  const G4double nanogray  = 1.e-9*gray;
  const G4double picogray  = 1.e-12*gray;

  new G4UnitDefinition("milligray", "milliGy" , "Dose", milligray);
  new G4UnitDefinition("microgray", "microGy" , "Dose", microgray);
  new G4UnitDefinition("nanogray" , "nanoGy"  , "Dose", nanogray);
  new G4UnitDefinition("picogray" , "picoGy"  , "Dose", picogray);

  // Register accumulable to the accumulable manager
  G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
  accumulableManager->RegisterAccumulable(fEdep);
  accumulableManager->RegisterAccumulable(fEdep2);

  edepHisto = new TH1F("EdepSi",
                       "Edep in Si",
                       1000, 0 , 1);
  edepHisto->GetXaxis()->SetTitle("edep [MeV]");
  edepHisto->GetYaxis()->SetTitle("counts");
  edepSiO2Histo = new TH1F("EdepSiO2",
                           "Edep in SiO2",
                           1000, 0 , 0.02);
  edepSiO2Histo->GetXaxis()->SetTitle("edep [MeV]");
  edepSiO2Histo->GetYaxis()->SetTitle("counts");
  eHisto = new TH1F("hKineticEnergy",
                    "Kinetic Energy of Betas",
                    1000, 0 , 2);
  eHisto->GetXaxis()->SetTitle("kinetic energy [MeV]");
  eHisto->GetYaxis()->SetTitle("counts");

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{ 
  delete fHistoManager;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* RunAction::GenerateRun()
{ 
  fRun = new Run();
  return fRun;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run*)
{ 
  // keep run condition
  if (fPrimary) { 
    G4ParticleDefinition* particle 
      = fPrimary->GetParticleGun()->GetParticleDefinition();
    G4double energy = fPrimary->GetParticleGun()->GetParticleEnergy();
    fRun->SetPrimary(particle, energy);
  }    
      
  //histograms
  //
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  if ( analysisManager->IsActive() ) {
    analysisManager->OpenFile();
  }     
  
  //inform the runManager to save random number seed
  //
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);  

  // reset accumulables to their initial values
  G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
  accumulableManager->Reset();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* run)
{
    G4int nofEvents = run->GetNumberOfEvent();
    if (nofEvents == 0) return;

    // Merge accumulables
    G4AccumulableManager* accumulableManager = G4AccumulableManager::Instance();
    accumulableManager->Merge();

    // Compute dose = total energy deposit in a run and its variance
    //
    G4double edep  = fEdep.GetValue();
    G4double edep2 = fEdep2.GetValue();

    G4double rms = edep2 - edep*edep/nofEvents;
    if (rms > 0.) rms = std::sqrt(rms); else rms = 0.;

    const DetectorConstruction* detectorConstruction
        = static_cast<const DetectorConstruction*>
        (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    G4double mass = detectorConstruction->GetScoringVolume()->GetMass();
    G4double dose = edep/mass;
    G4double rmsDose = rms/mass;
    G4cout << "Edep = " << edep / MeV << "MeV" << G4endl;
    G4cout << "Mass = " << mass / kg << "kg" << G4endl;

    // Run conditions
    //  note: There is no primary generator action object for "master"
    //        run manager for multi-threaded mode.
    const PrimaryGeneratorAction* generatorAction
        = static_cast<const PrimaryGeneratorAction*>
        (G4RunManager::GetRunManager()->GetUserPrimaryGeneratorAction());
    G4String runCondition;
    if (generatorAction)
    {
        //const G4ParticleGun* particleGun = generatorAction->GetParticleGun();
        //runCondition += particleGun->GetParticleDefinition()->GetParticleName();
        //runCondition += " of ";
        //G4double particleEnergy = particleGun->GetParticleEnergy();
        //runCondition += G4BestUnit(particleEnergy,"Energy");
    }

    // Print
    //
    if (IsMaster()) {
        G4cout
            << G4endl
            << "--------------------End of Global Run-----------------------";
    }
    else {
        G4cout
            << G4endl
            << "--------------------End of Local Run------------------------";
    }

    G4cout
        << G4endl
        << " The run consists of " << nofEvents << " "<< runCondition
        << G4endl
        << " Cumulated dose per run, in scoring volume : "
        << G4BestUnit(dose,"Dose") << " rms = " << G4BestUnit(rmsDose,"Dose")
        << G4endl
        << "------------------------------------------------------------"
        << G4endl
        << G4endl;

    ///////////////


    if (isMaster)
        fRun->EndOfRun();

    //save histograms
    //
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    if ( analysisManager->IsActive() ) {
        analysisManager->Write();
        analysisManager->CloseFile();
    } 

    TFile *output = new TFile("out.root", "RECREATE");
    edepHisto->Write();
    edepSiO2Histo->Write();
    eHisto->Write();
    TH1D *misc = new TH1D("misc", "0: nhits, 2: niel, 4: dose", 10, -0.5, 9.5);
    misc->SetBinContent(1, nhits);
    G4cout << nhits << " Hits " << G4endl
           << niel  << " neq in Si" << G4endl
           << fdose << " J/kg (Gy) dose = "
           << fdose / 3.9208e-10 * joule / MeV << " MeV"
           << G4endl;
    misc->SetBinContent(3, niel);
    misc->SetBinContent(5, fdose);
    misc->Write();
    output->Close();


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::AddEdep(G4double edep)
{
  fEdep  += edep;
  fEdep2 += edep*edep;
}

void RunAction::AddNiel(G4double n)
{
    niel += n;
}

void RunAction::AddDose(G4double d)
{
    fdose += d;
}

void RunAction::FillEdepHisto(G4double d)
{
    edepHisto->Fill(d);
}

void RunAction::FillEdepSiO2Histo(G4double d)
{
    edepSiO2Histo->Fill(d);
}

void RunAction::FillEHisto(G4double d)
{
    eHisto->Fill(d);
}


