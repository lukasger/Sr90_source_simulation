//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: SteppingAction.cc 74483 2013-10-09 13:37:06Z gcosmo $
//
/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"

using namespace CLHEP;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(EventAction* eventAction)
    : G4UserSteppingAction(),
      fEventAction(eventAction),
      fScoringVolume(0)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
    if (!fScoringVolume) {
        const DetectorConstruction* detectorConstruction
                = static_cast<const DetectorConstruction*>
                (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
        fScoringVolume = detectorConstruction->GetScoringVolume();
    }

    // get volume of the current step
    G4LogicalVolume* volume
            = step->GetPreStepPoint()->GetTouchableHandle()
            ->GetVolume()->GetLogicalVolume();

    // check if we are in scoring volume
    if (volume == fScoringVolume) {
        // collect energy deposited in this step
        G4double edepStep = step->GetTotalEnergyDeposit();
        //        printf("Calling AddEdep(%f)\n",edepStep);
        fEventAction->AddEdep(edepStep);
    }


    //// From Simon
    G4double edep = step->GetTotalEnergyDeposit();
    if(edep <= 0) return;
    auto prePoint = step->GetPreStepPoint();
    bool isSio2 = prePoint->GetPhysicalVolume()->GetName() == "Oxide";
    bool isSi = prePoint->GetPhysicalVolume()->GetName() == "Silicon";

    int depth = 0 + isSio2;
    G4double dose_c = 0;
    int pid = step->GetTrack()->GetParticleDefinition()->GetPDGEncoding();
    if(isSio2) {
        G4double cubicVolume = prePoint->GetPhysicalVolume()->GetLogicalVolume()->GetSolid()->GetCubicVolume();
        G4double density = prePoint->GetMaterial()->GetDensity();
        dose_c    = edep/joule / ( density/kg * cubicVolume );
//        G4cout << "DEBUG: density/kg * cubicVolume = " << density/kg * cubicVolume << G4endl;
//        G4cout << "DEBUG: dose = " << dose_c << " J/kg * " << prePoint->GetWeight() << G4endl;
        dose_c *= prePoint->GetWeight();
        fEventAction->GetRunAction()->AddDose(dose_c);

        fEventAction->GetRunAction()->FillEdepSiO2Histo(edep);
    } else if (isSi) {
        if (step->IsFirstStepInVolume()) fEventAction->GetRunAction()->AddHit();

        double df = 0;
        if (step->IsFirstStepInVolume()) {
            df = damageFunction(step->GetTrack()->GetKineticEnergy());
        }
        fEventAction->GetRunAction()->AddNiel(df);
        fEventAction->GetRunAction()->FillEdepHisto(edep);
        if (step->IsFirstStepInVolume()) {
            fEventAction->GetRunAction()->FillEHisto(step->GetTrack()->GetKineticEnergy());
            if(step->GetTrack()->GetDynamicParticle()->GetPDGcode() != 11)
                G4cout << "Hit in Si, PID = " << step->GetTrack()->GetDynamicParticle()->GetPDGcode() << G4endl;
        }

    }
    ////
}

double SteppingAction::damageFunction(double energy)
{
    if(energy <= df_e.front()) return df_n.front();
    if(energy >= df_e.back())  return df_n.back();

    auto index = std::distance(df_e.begin(), std::upper_bound (df_e.begin(), df_e.end(), energy));
    if (index > 1) {
        double frac = (energy - df_e.at(index-1)) / (df_e.at(index) - df_e.at(index-1));
        return df_n.at(index-1) + frac * (df_n.at(index) - df_n.at(index-1));

    } else return df_n.front(); // we should never get here
    //double niel2df = std:pow(energy, 0.98390033838489543) * 1.8e-7;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

